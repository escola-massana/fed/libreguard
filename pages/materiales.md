## ESPAÑOL

**MATERIALES VÁLIDOS**

Plásticos **no porosos** (los poros pueden acumular bacterias y virus aunque la superfície se desinfecte) que resisten el **lavado con alcohol etílico**.

En planchas de **0.8 - 1 mm** de grosor.

|  | Troqueladora | Láser |
|---|---|---|
| Pantalla transparente | PVC, PET | PET |
| Estructura | PVC, PET, PP (Plakene, Polipropileno) | PET, PP (Plakene, Polipropileno) |

Elegir PP cuando sea posible, ya que a diferencia de los otros es también esterilizable por autoclave.



Materiales **NO válidos**:

| Material | Razón |
| --- | --- |
| PLA | Porosidad |
| Acetato | Se degrada en contacto con alcohol |


Queda por determinar otros tipos de plástico.
Página web donde encontrar información sobre compatibilidad de plásticos con químicos, en un enlace dentro de cada material (asegurarse de que es compatible con alcohol etílico): http://kmac-plastics.net/






## ENGLISH

**VALID MATERIALS**

**Non-porous** plastics (bacteria and viruses can accumulate inside the pores even if the surface is disinfected) that resist **cleaning with ethyl alcohol**.

In sheets of **0.8 - 1 mm** thickness.

|  | Die cutter | Laser |
|---|---|---|
| Transparent guard | PVC, PET | PET |
| Structure | PVC, PET, PP (Plakene, Polypropylene) | PET, PP (Plakene, Polypropylene) |

Choose PP whenever possible, since unlike the other plastics it's also sterilisable by autoclave.


**NON-valid** materials:

| Material | Reason |
| --- | --- |
| PLA | Porosity |
| Acetato | Degrades in contact with alcohol |


Other types of plastic yet to be determined.
Website with information about chemical compatibility of several plastics, in a link inside of each material (make sure it's compatible with ethyl alcohol): http://kmac-plastics.net/