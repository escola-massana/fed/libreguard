## ESPAÑOL

El diseño permite la desinfección en el punto de recepción, antes del ensamblado. 
Se recomiendo desmontar (2 minutos) y desinfectar (2 minutos) después de varias horas de uso.

'''Desinfectar con alcohol etílico o en su defecto mediante lejía reducida al 10% en agua.'''

Autoclave:

| Material | Apto |
| ------ | ------ |
| PET | NO |
| PVC | NO |
| PP (Plakene, Polipropileno) | SÍ |

Para otros métodos de esterilización, consultar compatibilidad en esta tabla: https://www.industrialspec.com/resources/plastics-sterilization-compatibility.


## ENGLISH

The design allows for disinfection at the point of receipt, prior to assembly. 
It is recommended to disassemble (2 minutes) and disinfect (2 minutes) after several hours of use.

'''Disinfect with ethyl alcohol, or failing that, with a 10% bleach in water.'''

Autoclave:

| Material | Apt |
| ------ | ------ |
| PET | NO |
| PVC | NO |
| PP (Plakene, Polypropylene) | YES | 

For other sterilization methods, consult the following table: https://www.industrialspec.com/resources/plastics-sterilization-compatibility.
