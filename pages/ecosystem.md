## ESPAÑOL

Este diseño permite aprovechar láminas de plástico con gran eficiencia, produciendo muy poca merma.

Cuidado! Este producto es de plástico.  
Intentad reutilizarlo tanto como podáis, y si nos es posible destinadlo a reciclaje.
:)


## ENGLISH

This design maximizes the ammount of waste material.

Be careful! This product is made of plastic.  
Try to reuse it as much as you can, and if not possible then please recycle it.
:)
