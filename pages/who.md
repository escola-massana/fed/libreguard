# CATALÀ

Aquest projecte és un homenatge a la gent que cada dia de l'any dedica la seva vida la cura i atenció de la resta.

A l'àrea Barcelona han contribuit de forma altruista i de moltes formes diferents:

Materials:
 * [CODA](http://coda-office.com/)
 * [Rido](http://www.ridoenvase.com/) - sou els millors!
 * [Melnik](http://www.melnik.es/index.php/es/) - plakene a preu de cost
 * [XR66](http://www.xr66.com)  - donació de 10k viseres de PET
 * [AOE Plastics](http://aoeplastics.com)
 * Rajapack

Producció (tall laser):
 * [Rido](http://www.ridoenvase.com/)
 * [TMDC](www.tmdc.es)
 * [Escola Massana](https://www.escolamassana.cat/)
 * [Rafael Madrid](https://www.mediodesign.com/)
 * [Juan Pablo Quintero](https://www.mediodesign.com/)
 * [Maria León ](https://www.mediodesign.com/)
 * [LCI](https://www.lcibarcelona.com/) - [Citlali aka Turbulente](http://turbulente.net/) + Andrés C. M.
 * [Fabbercorp](http://fabbercorp.com/)
 * [Sumeplast](http://www.sumeplastsl.com/)
 * [Digitall](http://www.digitall.es/)
 * [Sergi Ubach](http://www.troquelsgeser.com)
 * [Inti Velez](http://www.wandabarcelona.com)
 * A3Studio
 * BUIT taller
 * Plastics Sant Jordi
 * Fab Casa del Mig
 * [Troqueles Rubio](https://troquelesrubio.es/)
 * [Treballs Plàstics](https://www.treballsplastics.com/)
 
Logística:
 * [Valeria Ustarez](http://coudre.studio)
 * [Jude Serena](http://coudre.studio)
 * [Mireia c. Saladrigues](http://www.mireiasaladrigues.com/)
 * David Haro
 * Javier Notivol
 * Pol
 * [Pep Tornabell](http://coda-office.com/)
 * [Raul Nieves](http://becoming.network)
 * Alejandro Moscoso
 * Marc Casamajo
 * [TransfoLab BCN](http://www.transfolabbcn.com)
 * Sylvia Market
 * Bartosz Zygmunt

Comunicació:
 * [Andreu Belsunces](https://becoming.network)
 * [Gus](http://fabbercorp.com/)
 * David Haro
 * Javier Notivol
 * [Anne Claire Lamy](http://anneclairelamy.fr/)
 * Teresa Roig

Disseny:
 * [Pep Tornabell](http://coda-office.com/)
 * [Raul Nieves](http://coudre.studio)
 * [Jude Serena](http://coudre.studio)
 * [Valeria Ustarez](http://coudre.studio)
 * Matt
 * [Juan Pablo](https://www.mediodesign.com/)




## ENGLISH

This project is a collaborative -design, communication, production and logistics- work where many people (beyond the list) contributed.
Spawn your own community!
