## ESPAÑOL

El coste estimado de una máscara es de 0.2€ en españa.  
De todos modos os recomendamos intentar poneros en contacto con fabricantes, vendedores mayoristas, o fábricas donde se usen estos [materiales](pages/materiales.md) ya que es muy probable que os los regalen.

:)


## ENGLISH

The estimated cost of a mask is 0.2USD in the USA.  
However, we recommend you try to get in touch with manufacturers, wholesalers, or factories where these [materials](pages/materiales.md) are used since it is very likely that they give you material for free. 

:)
