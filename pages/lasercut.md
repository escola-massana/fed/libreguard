## ESPAÑOL

El diseño de máscara Libreguard está pensado para cortarse con una **máquina de control numérico de corte laser** industrial o semiindustrial con un míínimo de 60W.  
Por lo tanto os recomendamos buscar alguna fábrica que os pueda ayudar. Vale más la pena perder un poco de tiempo en encontrar una forma de producción industrial que en hacer pequeñas producciones.

Con qualquier máquina de corte láser podréis cortar los [materiales](pages/materiales.md) recomendados.  

**Éste es el** <a id="raw-url" href="https://gitlab.com/escola-massana/fed/libreguard/-/archive/master/libreguard-master.zip?path=sources">ARCHIVO</a> (version mk6) **en formato dxf, válido para cualquier programa de preparación de corte.**

**Parámetros**

Los parámetros de corte láser que estamos usando son los siguientes:

* Grabado: 
  * potencia(100W): 30%
  * velocidad: 200 mm/s
* Corte: 
  * potencia(100W): 95%
  * velocidad: 65 mm/s

'''Aseguraos de cortar una prueba antes de cortar varias para verificar que las tolerancias (el tamaño de los agujeros que incorpora el diseño) funcionan en vuestra máquina-material. Adaptadlos como sea conveniente.'''

## ENGLISH

The Libreguard browguard is designed to be cut with an industrial or semi-industrial **laser cutting numeric control machine** with a minimum of 60W. 
Therefore we recommend you to look for a factory that can help you. It is worth spending a little time to find a way of industrial production than to make small productions.

With any laser cutting machine you can cut the recommended [materials](pages/materiales.md).  

**This is the** <a id="raw-url" href="https://gitlab.com/escola-massana/fed/libreguard/-/archive/master/libreguard-master.zip?path=sources">FILE</a> (version mk6) **in dxf format, valid for any lasercutting software.**

**Parameters**

The laser cut parameters we are using are the following:

* Engraving: 
  * power(100W): 30%
  * speed: 200 mm/s
* Cutting: 
  * power(100W): 95%, 
  * speed: 65 mm/s

'''Make sure you cut a test before cutting several guards to verify that the tolerances (the size of the holes that the design incorporates) work in your machine-material. Adapt as necessary.'''

---

![alt text](https://gitlab.com/escola-massana/fed/libreguard/-/raw/c1df786660fe0de9028c49c82843c2a55329162a/pictures/dxf_file.jpg "imagen de referencia")

