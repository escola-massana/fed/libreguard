## ESPAÑOL

Además de conseguir cortar las máscaras, para que el proyecto funcione en su totalidad como infraestructura para proveerlas hace falta una buena organización de logística. Esta es la forma en la que hemos gestionado diariamente el recorrido de los pedidos, teniendo en cuenta que establecimos un punto donde debían pasar a recogerlos; si tenéis voluntaries que puedan repartirlos, deberéis adaptarlo.

En resumen:
Pedido entra por formulario --> se avisa cada día por telegram a quienes pueden pasar a recogerlo al día siguiente --> entrega.

#### 1. CHAT TELEGRAM DE PEDIDOS
La usuaria se une en el chat. Un bot, como [BanHammer](https://banhammerbot.ml/) o [Group Butler](https://techwebsites.net/group-butler-tutorial/), le da la bienvenida con un mensaje conteniendo enlaces a la web para la información de montaje y desinfección, y al formulario para hacer un pedido.

#### 2. FORMULARIO DE GOOGLE
Un formulario de Google ([https://www.google.com/forms/about/](https://www.google.com/forms/about/)) recoge sus datos. Es conveniente detallar el proceso que siguen los pedidos al principio como introducción del formulario, para que la usuaria sepa qué esperar; si no, las preguntas constantes por el chat os harán menos productives.
Los datos que nosotres pedimos han sido:
* Nombre y apellidos del contacto
* Teléfono del contacto
* Número y tipo de credencial del contacto (especificar que pueda ser cualquier tipo de credencial: DNI, NIE, pasaporte, carnet de biblioteca, número de tarjeta, carnet de alguna afiliación... queremos ser inclusives con quien esté en situación irregular. Esto es para que no haya suplantaciones de identidad al recoger las máscaras. Indicar que pueden traer una fotocopia si pasa otra persona a su nombre)
* Nombre del centro/institución
* Tipo de centro/institución (hospital, urgencias, uci, ambulancia, ambulancia de urgencias, centro de atención primaria, residencia, cuidadora, otros)
* Unidades necesarias
* Grado de afectación de COVID-19 en el centro (no obligatorio)

Además, también pedimos que trajeran a la recogida cualquier tipo de documento o prueba que nos diera a entender que tenían necesidad de las máscaras.

**CUIDADO**: Para mandarlo a les usuaries, no uséis el enlace de edición del documento, sino el de responderlo (botón SEND > send via link). Si editáis el formulario, tenéis que generar un nuevo link y editar el mensaje del bot de bienvenida para que enlace al nuevo.
    
#### 3. HOJA DE CÁLCULO
Los datos del formulario se vuelcan diariamente en una hoja de cálculo:

--> Desde la página de edición del formulario, pulsando arriba en "respuestas".

--> Arriba a la derecha, clicar el icono verde "create spreadsheet". Esto actualiza la hoja de cálculo con las nuevas respuestas.

--> Se añaden columnas y filas (quedarán guardadas aunque volváis a pulsar el botón para generar la hoja):
* filas para separar pedidos **"entregados"**, pedidos **"pendientes"**, pedidos **"a repartir al día siguiente"**, y pedidos **"no entregados"** (para distinguir los que no los pasaron a buscar -por lo que entendemos que no es tan urgente, y por lo tanto tendrán menos prioridad- de los "pendientes").
* una columna con la fecha para apuntar las **máscaras que se entregarán** ese día a cada centro
* una columna con resta automática para las que les **faltan**, teniendo en cuenta que no siempre se van a poder dar todas las que pidieron o se repartirán más equitativamente entre más centros.

#### 4. RECUENTO DE MÁSCARAS
Para poderlas repartir entre los centros y avisarles de que las pasen a recoger, se hace un recuento diario para saber cuántas habrá disponibles para el día siguiente. Si sois varios sitios produciéndolas, hay que tener une voluntarie que pase a recogerlas para llevarlas todas al punto de recogida, o bien podéis buscar la forma de funcionar con varios puntos de recogida, haciendo que la usuaria seleccione en el formulario el que prefiera.

#### 5. DIVISIÓN
Una o dos personas del equipo se encargan de ordenar los pedidos según la prioridad, teniendo en cuenta no sólo la fecha del pedido sino el tipo de centro y la cantidad de máscaras que necesitan, así como la gravedad de su situación. Se pasan los más urgentes a **"repartir al día siguiente"** y se les asigna un número de máscaras a repartir dependiendo de las que pidieron y las que hay disponibles del recuento. También se tiene en cuenta la columna **"faltan"**, aunque al centro ya se le hayan repartido.

#### 6. NOTIFICACIÓN
Por medio de otro canal de Telegram (aseguráos de hacer un canal unidireccional en lugar de un chat, separado del chat de bienvenida para que las notificaciones de entregas no queden enterradas por mensajes) al que ya se pidió a la usuaria que se uniera (en el bot o en el formulario) se publica una lista de los centros que podrán pasar a buscar su pedido al día siguiente, así como la franja horaria en la que hacerlo y la dirección.

#### 7. ENTREGA Y VERIFICACIÓN
El equipo de entrega en el punto de recogida actualiza la hoja de cálculo moviendo los pedidos de **"repartir al día siguiente"** a **"entregados"**, o si no se pasaron a buscar, a **"no entregados"**.


---

## ENGLISH

In addition to managing to cut the masks, a good logistical organization is needed for the project to function fully as an infrastructure for providing them. This is the way we have managed the orders on a daily basis, taking into account that we established a point where they had to be picked up; if you have volunteers who can distribute them, you will have to adapt to that.

In short:
Order enters by form --> those who can pick up the order the next day are notified through Telegram --> pick up.

#### 1. TELEGRAM CHAT FOR ORDERS
The user joins the chat. A bot, such as [BanHammer](https://banhammerbot.ml/) or [Group Butler](https://techwebsites.net/group-butler-tutorial/), welcomes her with a message containing links to the website for assembly and disinfection information, and to the form for placing an order.

#### 2. GOOGLE FORM
A Google form ([https://www.google.com/forms/about/](https://www.google.com/forms/about/)) collects her data. It is convenient to detail the process that the orders follow at the beginning of the form, so that the user knows what to expect; otherwise, the constant questions through the chat will make you less productive.
The data that we ask for has been:
* Name and surname of the contact
* Contact phone number
* Number and type of credential of the contact (specify that it can be any type of credential: ID card, passport, library card, credit card number, affiliation cards... we want to be inclusive with whoever is in an irregular situation. This is so that there is no identity theft when collecting the masks. Indicate that they can bring a photocopy if another person comes to pick it up for them)
* Name of the center/institution
* Type of centre/institution (hospital, emergencies, icu, ambulance, emergency ambulance, primary care centre, residence, caregiver, others)
* Units required
* Degree of affectation of COVID-19 in the centre (not compulsory)

In addition, we also asked them to bring any kind of document or evidence that would suggest they need the masks.

**WARNING**: To send it to the users, do not use the edit link of the document, but the one to answer it (SEND button > send via link). If you edit the form, you have to generate a new link and edit the message of the welcoming bot so that it links to the new one.
    
#### 3. SPREADSHEET
The form data is transferred daily into a spreadsheet:

--> From the edit page of the form, click on "answers" at the top.

--> At the top right, click on the green "create spreadsheet" icon. This updates the spreadsheet with the new answers.

--> Columns and rows are added (they will be saved even when you press the button to generate the sheet again):
* rows to separate **"delivered"** orders, **"pending"** orders, **"to be delivered the next day"** orders, and **"undelivered"** orders (to distinguish those that were not picked up -so we understand that it is not so urgent, and therefore they will have less priority- from the "pending" ones).
* a column with the date to write down the **masks that will be delivered** that day to each centre
* a column with automatic subtraction for how many masks each centre is **missing**, taking into account that it will not always be possible to give them all masks they asked for since they will usually be distributed more equally among more centres.

#### 4. MASK COUNT
In order to distribute masks among the centers and to notify them that they can be picked up, a daily mask count is made to know how many will be available for the following day. If there are several places producing them, a volunteer must pick them up and take them all to the single collection point, or you can look for a way to work with several collection points, making the user select the one she prefers on the form.

#### 5. DIVISION
One or two people from the team are responsible for ordering the orders according to priority, taking into account not only the date of the order but also the type of centre and the quantity of masks needed, as well as the seriousness of the situation. The most urgent ones are passed on to **"deliver the next day"** and are assigned a number of masks to be delivered depending on the masks ordered and those available from the count. The "missing" column is also taken into account, even if the centre has already been given them.

#### 6. NOTIFICATION
By means of another Telegram channel (make sure to make it a one-way channel instead of a chat, separate from the welcoming chat so that delivery notifications are not buried by messages) to which the user has already been asked to join (via the bot or the form), a list of the centres that will be able to pick up her order the next day is published, as well as the time slot in which to do so and the address.

#### 7. DELIVERY AND VERIFICATION
The delivery team at the collection point updates the spreadsheet by moving orders from **"to be delivered the next day"** to **"delivered"**, or if not picked up, to **"undelivered"**.