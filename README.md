# [LIBREGUARD](http://libreguard.care) 
(librevisor)

### ESPAÑOL
Libreguard es una **máscara DIY libre** para frenar el avance del **COVID-19**.
Encuentra una máquina de corte láser o troqueladora, lámina de plástico y haz 1000 máscaras al día!

### ENGLISH
Libreguard is a **libre DIY browguard** designed to stop the **COVID-19** spread.
Find a lasercut or die cutting machine, plastic sheet and produce 1000 masks a day!

![alt text](pictures/resum2.png "matt")

## INSTAGRAM [#libreguard](https://www.instagram.com/explore/tags/libreguard/?hl=en)

### PEDIDOS/COMANDES (zona Barcelona, SP)
telegram: https://t.me/librevisor
formulario de pedidos: https://forms.gle/P4RtUdjL1TkNrSMB9

### ESPAÑOL
* Producción [rápida](pages/lasercut.md) (1 minuto 20 segundo) y [barata](pages/cost.md) (0.20€) de una hoja de un solo material.
* En [materiales](pages/materiales.md) sanitarios baratos.
* Fácil y rápido [montaje](pages/assembly.md) : 2 minutos.
* Fácil y rápida [limpieza](pages/cleaning.md), frontal y estructura reultilizables.
* [Con muy poco material desperdiciado](pages/ecosystem.md)
* [Logística](pages/logistica.md) autogestionada con herramientas gratuitas, para distribuirlas además de producirlas.
* [Mejorásteis el diseño?](pages/development.md) :)

### ENGLISH

* Its designed to be produced [fast](pages/lasercut.md) (1 minute 20 sec) and [cheap](pages/cost.md) (0.20 USD) and from a single material sheet.
* In sanitary-compatible [cheap materials](pages/materiales.md).
* Easy and fast to [assemble](pages/assembly.md): 2 minutes.
* Easy and fast to [clean](pages/cleaning.md), reusable structure and sheet.
* [With little waste](pages/ecosystem.md)
* Self-managed [supply chain](pages/logistica.md) with free tools, to distribute in addition to producing them.
* [Did you improve the design?](pages/development.md) :)

---

LICENSE GPLv3 - this is a libre product for a libre world

https://www.gnu.org/licenses/gpl-3.0.html

---

![alt text](/pictures/img7.jpg "matt")
![alt text](/pictures/img8.jpg "matt")
![alt text](/pictures/img9.jpg "matt")



