# Summary

* [START<---](README.md)
## MANUAL
* [materiales](pages/materiales.md)
* [lasercut](pages/lasercut.md)
* [logística](pages/logistica.md)
* [assembly](pages/assembly.md)
* [cleaning](pages/cleaning.md)
* [sostenibilidad](pages/ecosystem.md)

## ---

* [GALERÍA](pages/gallery.md)
* [quiénes somos](pages/who.md)
* [versiones](pages/development.md)

### version mk6

### 2020/03/23
